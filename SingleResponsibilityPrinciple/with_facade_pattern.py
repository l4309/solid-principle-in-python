class PersonDB:
    def save(cls, person: object) -> str:
        return f"Person ==> `{person}` saved."


class Person:
    def __init__(self, name: str) -> None:
        self.name: str = name
        self.db = PersonDB()
    
    def __repr__(self) -> str:
        return f"Person - {self.name}"
    
    def save(self) -> str:
        return self.db.save(self)


if __name__ == "__main__":
    p1: Person = Person("John 1")
    p2: Person = Person("John 2")
    p3: Person = Person("John 3")

    print(p1)
    print(p2)
    print(p3)

    p1_save: str = p1.save()
    p2_save: str = p2.save()
    p3_save: str = p3.save()
    
    print(p1)
    print(p2)
    print(p3)
