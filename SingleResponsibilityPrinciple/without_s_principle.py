class Person:
    def __init__(self, name: str) -> None:
        self.name: str = name
    
    def __repr__(self) -> str:
        return f"Person - {self.name}"
    
    @classmethod
    def save(cls, person: object) -> str:
        return f"Person ==> `{person}` saved."


if __name__ == "__main__":
    p1: Person = Person("John 1")
    p2: Person = Person("John 2")
    p3: Person = Person("John 3")

    print(p1)
    print(p2)
    print(p3)

    p1_save: str = p1.save(p1)
    p2_save: str = p2.save(p2)
    p3_save: str = p3.save(p3)

    print(p1_save)
    print(p2_save)
    print(p3_save)