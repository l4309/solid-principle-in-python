# SOLID Principle

`SOLID` is an abbreviation that stands for `five software design principles`. Solid principle used on `Object Oriented Programming (OOP)`. It is combination of 5 principles denoted as `S, O, L, I, D`. These five design principles make your software designs more maintainable and flexible.

## S Principle

>  __`S`__ known as **_Single Responsibility Principle_**

## Introduction to the single responsibility principle

> The single responsibility is the first principle in the SOLID principles.

The single responsibility principle (SRP) states that every  `class`, `method`, and `function` should have only one job or one reason to change.

The purposes of the single responsibility principle are to:
-   Create high cohesive and robust classes, methods, and functions.
-   Promote class composition
-   Avoid code duplication

Let’s take a look at the following  `Person`  class:
```python
class Person:
	def __init__(self, name: str) -> None:
		self.name: str = name

	def __repr__(self) -> str:
		return  f"Person - {self.name}"

	@classmethod
	def save(cls, person: object):
		return  f"Person ==> `{person}` saved."

if __name__ == "__main__":
	p1 = Person("John 1")
	p2 = Person("John 2")
	p3 = Person("John 3")
	
	print(p1)
	print(p2)
	print(p3)

	p1_save = p1.save(p1)
	p2_save = p2.save(p2)
	p3_save = p3.save(p3)
	
	print(p1_save)
	print(p2_save)
	print(p3_save)
```

This  `Person`  class has two jobs:
-   Manage the person’s property.
	```python
	def __init__(self, name: str) -> None:
		self.name: str = name
	```
-   Store the person in the database.
	```python
	@classmethod
	def save(cls, person: object):
		return  f"Person ==> `{person}` saved."
	```

Later, if you want to save the `Person` into different storage such as a file, you’ll need to change the `save()` method, which also changes the whole `Person` class.

To make the  `Person`  class conforms to the single responsibility principle, you’ll need to create another class that is in charge of storing the  `Person`  to a database. For example:
```python
class Person:
	def __init__(self, name: str) -> None:
		self.name: str = name
		
	def __repr__(self) -> str:
		return  f"Person - {self.name}"


class PersonDB:
	def save(cls, person: object):
		return  f"Person ==> `{person}` saved."


if __name__ == "__main__":
	p1 = Person("John 1")
	p2 = Person("John 2")
	p3 = Person("John 3")
	
	print(p1)
	print(p2)
	print(p3)

	db = PersonDB()

	p1_save = db.save(p1)
	p2_save = db.save(p2)
	p3_save = db.save(p3)

	print(p1_save)
	print(p2_save)
	print(p3_save)
```

In this design, we separate the  `Person`  class into two classes:  `Person`  and  `PersonDB`:
-   The Person class is responsible for managing the person’s properties.
-   The PersonDB class is responsible for storing the person in the database.

In this design, if you want to save the `Person` to different storage, you can define another class to do that. And you don’t need to change the `Person` class.

When designing classes, you should put related methods that have the same reason for change together. In other words, you should separate classes if they change for different reasons.

This design has one issue that you need to deal with two classes: `Person` and `PersonDB`.

To make it more convenient, you can use the facade pattern so that the `Person` class will be the facade for the `PersonDB` class like this:

```python
class PersonDB:
	def save(cls, person: object):
		return  f"Person ==> `{person}` saved."


class Person:
	def __init__(self, name: str) -> None:
		self.name: str = name
		self.db = PersonDB()
		
	def __repr__(self) -> str:
		return  f"Person - {self.name}"

    def save(self):
        self.db.save(self)


if __name__ == '__main__':
    p1 = Person("John 1")
	p2 = Person("John 2")
	p3 = Person("John 3")
	
	print(p1)
	print(p2)
	print(p3)
	
	p1_save = p1.save()
	p2_save = p2.save()
	p3_save = p3.save()

	print(p1_save)
	print(p2_save)
	print(p3_save)
```

## Summary

-   The single responsibility principle (SRP) states that every class, method, or function should have only one job or one reason to change.
-   Use the single responsibility principle to separate classes, methods, and functions with the same reason for changes.